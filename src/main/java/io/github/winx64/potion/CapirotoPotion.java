package io.github.winx64.potion;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.potion.PotionType;

public final class CapirotoPotion extends JavaPlugin {

	private static final String CAPIROTO_NAME = ChatColor.GOLD + "Poção do Capiroto";
	private static final ItemStack CAPIROTO_POTION;

	private static final String SYNTAX = ChatColor.RED + "Sintaxe: /capiroto <jogador> [quantidade]";
	private static final String PLAYER_NOT_FOUND = ChatColor.RED + "O jogador '%s' não foi encontrado!";
	private static final String INVALID_AMOUNT = ChatColor.RED + "A quantidade deve ser um numero entre 1 e 64!";
	private static final String PLAYER_RECEIVED_POTION = ChatColor.GREEN + "Você deu " + ChatColor.GRAY + "%d "
			+ CAPIROTO_NAME + " " + ChatColor.GREEN + "para " + ChatColor.RESET + "%s";
	private static final String YOU_RECEIVED_POTION = ChatColor.GREEN + "Você recebeu " + ChatColor.GRAY + "%d "
			+ CAPIROTO_NAME;

	static {
		ItemStack capirotoPotion = new Potion(PotionType.FIRE_RESISTANCE).toItemStack(1);
		PotionMeta meta = (PotionMeta) capirotoPotion.getItemMeta();
		meta.addCustomEffect(new PotionEffect(PotionEffectType.FIRE_RESISTANCE, 20 * 60 * 8, 0), true);
		meta.addCustomEffect(new PotionEffect(PotionEffectType.SPEED, 20 * 60 * 4, 1), true);
		meta.addCustomEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 20 * 60 * 4, 1), true);
		meta.addCustomEffect(new PotionEffect(PotionEffectType.REGENERATION, 20 * 60 * 2, 3), true);
		meta.addCustomEffect(new PotionEffect(PotionEffectType.ABSORPTION, 20 * 60 * 8, 0), true);
		meta.addCustomEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 20 * 60 * 8, 0), true);

		meta.setDisplayName(CAPIROTO_NAME);
		capirotoPotion.setItemMeta(meta);

		CAPIROTO_POTION = capirotoPotion;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String alias, String[] args) {
		if (args.length < 1) {
			sender.sendMessage(SYNTAX);
			return true;
		}

		Player target = Bukkit.getPlayer(args[0]);
		if (target == null) {
			sender.sendMessage(String.format(PLAYER_NOT_FOUND, args[0]));
			return true;
		}

		int amount = 1;
		if (args.length > 1) {
			try {
				amount = Integer.valueOf(args[1]);
			} catch (NumberFormatException e) {
				sender.sendMessage(INVALID_AMOUNT);
				return true;
			}
		}

		if (amount < 1 || amount > 64) {
			sender.sendMessage(INVALID_AMOUNT);
			return true;
		}

		ItemStack capirotoPotion = CAPIROTO_POTION.clone();
		capirotoPotion.setAmount(amount);

		target.getInventory().addItem(capirotoPotion);
		sender.sendMessage(String.format(PLAYER_RECEIVED_POTION, amount, target.getDisplayName()));
		target.sendMessage(String.format(YOU_RECEIVED_POTION, amount));
		return true;
	}
}
